python3-lxc (1:5.0.0-2) unstable; urgency=medium

  * d/control:
    - Update Standards-Version to 4.7.0 (no changes needed)
    - Update Build-Depends pkg-config => pkgconf
    - Removed hard-coded Depends on liblxc1 (Closes: #1068937)
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Sun, 14 Apr 2024 01:13:37 +0000

python3-lxc (1:5.0.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + debian/rules: Drop --fail-missing argument to dh_missing, which is now the
      default.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update standards version to 4.6.0, no changes needed.

  [ Mathias Gibbens ]
  * New upstream release
  * d/control:
    - Add myself to Uploaders
    - Bump Standards-Version (no changes needed)
    - Add Rules-Requires-Root: no
  * d/copyright:
    - Correct license; it has always been LGPL 2.1+, not LGPL 2+
    - Add missing files with an Oracle copyright
    - Add myself
  * Remove deprecated (and unneeded) needs-recommends from d/t/control
  * Add d/gbp.conf and d/salsa-ci.yml
  * Update d/u/metadata

  [ Stéphane Graber ]
  * Add simple testsuite for the package, from Ubuntu

 -- Mathias Gibbens <gibmat@debian.org>  Wed, 25 Jan 2023 22:45:27 +0000

python3-lxc (1:3.0.4-1) unstable; urgency=medium

  * New upstream release 3.0.4
  * Switch to debhelper-compat (level 12)
  * Bump Standards-Version to 4.4.0
  * Drop the version statement on {Build-,}Dependency on lxc-dev and liblxc1

 -- Pierre-Elliott Bécue <peb@debian.org>  Thu, 22 Aug 2019 13:34:23 +0200

python3-lxc (1:3.0.3-1) unstable; urgency=medium

  * New upstream release 3.0.3
  * Release to unstable

 -- Pierre-Elliott Bécue <peb@debian.org>  Tue, 04 Dec 2018 08:42:48 +0100

python3-lxc (1:3.0.2-1~exp+2) experimental; urgency=medium

  * d/rules: updated
  * d/python3-lxc.install => d/python3-lxc.examples
  * d/control: remove a build-dep on python3.7-dev

 -- Pierre-Elliott Bécue <peb@debian.org>  Thu, 29 Nov 2018 22:56:57 +0100

python3-lxc (1:3.0.2-1~exp+1) experimental; urgency=medium

  * New source package as upstream extracted python3-lxc from the main lxc
    project. Hence, no ITP to close.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 17 Nov 2018 14:19:19 +0100
